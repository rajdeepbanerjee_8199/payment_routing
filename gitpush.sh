#set paths
DATE=`date +%Y%m%d`
BASE_PATH='/Users/8199/Work/payment_routing'

#push bin-routing files
bin_path="$BASE_PATH/modelTrain/cards/model_out/$DATE"
cp $bin_path/bin_list200.csv $BASE_PATH/rss/rss/input/
cp $bin_path/bin_pgs.csv $BASE_PATH/rss/rss/input/
rm $BASE_PATH/rss/rss/pickle_files/*
cp $bin_path/pickle_files/* $BASE_PATH/rss/rss/pickle_files/
cd $BASE_PATH/rss
git config --global credential.helper cache
git add -A
git commit -m "updated files and .pkls for bin based routing"
git pull
git push origin master

#push bank-routing files
nb_path="$BASE_PATH/modelTrain/netbanking/model_out/$DATE"
cp $nb_path/bankid_list.csv $BASE_PATH/rss/rss/input/
cp $nb_path/bank_pgs.csv $BASE_PATH/rss/rss/input/
rm $BASE_PATH/rss/rss/nb_pickle_files/*
cp $nb_path/nb_pickle_files/* $BASE_PATH/rss/rss/nb_pickle_files/
cd $BASE_PATH/rss
git config --global credential.helper cache
git add -A
git commit -m "updated files and .pkls for netBanking routing"
git pull
git push origin master
