#!/bin/python
'''
    Module to submit query to ddp
    Queries are executed async.
    After submitting poll till it completes

    TODO
    =====
    1. Handle AWS errors
    2. Write a wiki doc
    3. Change to production HOST as when it is available
'''
import urllib.request,urllib.parse,urllib.response,json,time,argparse,wget

DEBUG=0
SLEEP_TIME=60

#DDP_API_HOST='http://54.251.230.158/executor-api/rest/v1/queryExecutor'
DDP_API_HOST='http://bi-ddp-service.myntra.com/executor-api/rest/v1/queryExecutor'

DDP_SUBMIT_QUERY=DDP_API_HOST+"/submit"
DDP_CHECK_STATUS=DDP_API_HOST+"/%s/status"
DDP_DOWNLOAD_RESULT=DDP_API_HOST+"/%s/download"

def post(url,data,headers,debug=0):
    ''' HTTP POST REQUEST. Returns string response '''
    req=urllib.request.Request(url,urllib.parse.urlencode(data).encode("utf-8"),headers)
    # httpHandler = urllib.HTTPHandler()
    # httpHandler.set_http_debuglevel(debug)
    # opener = urllib.build_opener(httpHandler)
    response = urllib.request.urlopen(req)
    return response

def get(url,debug=0):
    ''' HTTP GET REQUEST. Returns string response '''
    req=urllib.request.Request(url)
    # httpHandler = urllib.HTTPHandler()
    # httpHandler.set_http_debuglevel(debug)
    # opener = urllib.build_opener(httpHandler)
    response = urllib.request.urlopen(req)
    return response


def downloadFile(url,outDir):
    ''' Download file to specified directory. Returns filename '''
    url=url.decode("utf-8")
    return wget.download(url,outDir)

def submitQuery(query,user,
            query_name="my_query",
            query_description=""):
    '''
        Submits query to ddp
        Returns (True,<queryId>) if success
        else
        Returns (False,<failureMessage>)
    '''

    data={"query":query,"query-name":query_name,\
        "query-description":query_description,"unload":"true"}
    data={};
    data['query']=query
    data['query-name']=query_name
    data['query-description']=query_description
    data['unload'] ='true'

    headers={"Content-Type":"application/x-www-form-urlencoded",\
    "submitted-by":user}

    response=post(DDP_SUBMIT_QUERY,data,headers,DEBUG).read()

    response=json.loads(response)
    if response['success']:
        return response['queryId']
    raise Exception("Query Failed:%s" % response['failureMessage'])

def checkStatus(queryId):
    '''
        Returns status ("Completed"/"Running"/"Unknown")
    '''

    url=DDP_CHECK_STATUS % queryId

    response=get(url,DEBUG).read()

    return json.loads(response)['queryStatus']

def downloadResult(queryId,outFileDir):
    '''
        downloads query result to provided
        path. Path must be writable
    '''
    url=DDP_DOWNLOAD_RESULT % queryId
    response=get(url,DEBUG).read()
    downloadUrl=response
    print(downloadUrl)
    #response is the link
    filename=downloadFile(downloadUrl,outFileDir)
    return filename

def testDDP():
    ''' Just a test '''
    query='select * from dim_product limit 10'
    user='kapil.ratnani@myntra.com'
    query_name='ddp wrapper test'

    print ("Submitting Query to DDP")
    qid=submitQuery(query,user,query_name)
    print ("Query submission success. QueryId:%s"%qid)
    while True:
        print ("Checking Query Status")
        if checkStatus(qid)=="COMPLETED":
            print ("Query completed. Downloading Result")
            filename=downloadResult(qid,'/tmp/')
            print ("\nFile Downloaded:%s"%filename)
            break;
        else:
            print ('Query still executing. Sleeping:%d seconds' % SLEEP_TIME)
            time.sleep(SLEEP_TIME)

def start(query,user,outDir,query_name):
    ''' Entry point for first time request'''
    print ("Submitting Query to DDP")
    #qid=submitQuery(query,user,query_name)
    qid = 590996;
    print ("Query submission success. QueryId:%s" % qid)

    return pollQueryId(qid,outDir)

def pollQueryId(qid,outDir):
    ''' Entry point for checking and downloading pending request '''
    filename=''
    while True:
        print ("Checking Query Status")
        if checkStatus(qid)=="COMPLETED":
            print ("Query completed. Downloading Result")
            filename=downloadResult(qid,outDir)
            print ("\nFile Downloaded:%s"%filename)
            break;
        else:
            print ('Query still executing. Sleeping:%d seconds' % SLEEP_TIME)
            time.sleep(SLEEP_TIME)
    return filename

if __name__=="__main__":

    argPrsr=argparse.ArgumentParser()

    queryGroup=argPrsr.add_mutually_exclusive_group(required=True)

    queryGroup.add_argument("--query","-q",nargs=1,
                        metavar="\"query\"",
                        help="Query to be executed under quotes. "
                        "Ex.\"select * from table\"",dest="query")

    queryGroup.add_argument("--queryid","-i",nargs=1,help="Query id of a finished or running"
                        " query. This will download if the query is finished",dest="queryid")

    argPrsr.add_argument("--user","-u",nargs=1,
                        metavar="myntra-email-id",
                        required=True,help="Must be myntra email id")
    argPrsr.add_argument("--dir","-d",nargs=1,
                        metavar="dir",default=["."], required=False,
                        help="Directory where results must be stored. "
                        " Default:current directory")
    argPrsr.add_argument("--name","-n",nargs=1,default=["my_query"],required=False,
                        help="Name of query. Default:my_query")



    argPrsr.add_argument("--verbose","-v",action="store_true",help="print http calls")
    args=argPrsr.parse_args()

    if args.verbose:
        DEBUG=1

    if args.query:
        start(args.query[0],args.user[0],args.dir[0],args.name[0])
    elif args.queryid:
        pollQueryId(args.queryid[0],args.dir[0])
