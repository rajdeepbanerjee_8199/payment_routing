import sys
import ddp, gzip
from time import sleep
from tqdm import tqdm
import os
import pandas as pd
from collections import Counter


def runDDPquery(query, QUERY_OUT_DIR, QUERY_USER, QUERY_NAME):
    ddp.start(query, QUERY_USER, QUERY_OUT_DIR, QUERY_NAME)

def halfHrPeriod(row):
    try:
        t1 = str(row['insert_time'])[:-2]
        if len(t1)==0:
            t1 = 0
        t1 = int(t1)
        t2 = int(str(row['insert_time'])[-2:])
        return t1*2 + t2//30
    except:
        return 0

def createFeatures1(QUERY_OUT_DIR, QUERY_NAME, filename, bankIDListFile, include_pgs = ['citrusblaze', 'payu', 'tekprocess','ccavenue_dl']):
    f0 = pd.read_table(gzip.open(QUERY_OUT_DIR + '/' + QUERY_NAME + ".gz"), sep=",", header=None)
    f0.columns=['insert_date','payment_gateway_name','payment_option','insert_time','payment_issuer',
                'card_bank_name','bin_number','amount','is_saved_card','is_complete']
    f0 = f0.reset_index()
    f0 = f0.drop('index', 1)
    f0['half_hr_period'] = f0.apply(halfHrPeriod, axis=1).astype(int)

    f0 = f0[f0.payment_gateway_name.isin(include_pgs)]
    #f0 = f0[~f0.payment_gateway_name.isin(exclude_pgs)]
    f0 = f0[f0.is_complete.isin([0, 1])]

    # BankName to bankId map
    df_map = pd.read_csv(bankIDListFile)
    df_map = df_map.rename(columns={'name': 'card_bank_name'})
    f0 = pd.merge(f0, df_map, on='card_bank_name', how='inner')
    remove_ids = [9, 43, 45, 46, 54, 56, 60, 61, 63, 64, 65]
    f0 = f0[~f0.id.isin(remove_ids)]
    #print f0.payment_gateway_name.unique()
    f0.to_csv(QUERY_OUT_DIR + '/' + filename + '.csv', index=False)
    os.remove(QUERY_OUT_DIR + '/' + QUERY_NAME + ".gz")
    return f0


def createFeatures2(df, bankIDListFile, QUERY_OUT_DIR, filename_sr):
    df = df[df['payment_issuer'].notnull()]
    df = df.sort_values(['insert_date', 'insert_time'], ascending=[False, False]).reset_index().drop('index', 1)
    df['half_hr_period_1'] = df['half_hr_period'].map(lambda x: '0' + str(x) if len(str(x)) == 1 else str(x))
    df['date_hour'] = df.apply(lambda x: '%s%s' % (x['insert_date'], x['half_hr_period_1']), axis=1).astype(int)

    bank_list = pd.read_csv(bankIDListFile,names=['id'])
    df0 = df[['card_bank_name', 'id', 'payment_gateway_name', 'date_hour', 'insert_time', 'is_complete']]
    cols = ['id', 'pgname', 'date_hour', 'successrate20', 'current_sr15']
    # calculate the half hourly success rates
    df_sr = pd.DataFrame(columns=(cols))

    for pi in tqdm(list(bank_list.id)):
        sleep(0.001)
        df1 = df0[df0['id'] == pi].reset_index().drop('index', 1)
        # print df1.shape
        for pg in df1['payment_gateway_name'].unique():
            df2 = df1[(df1['payment_gateway_name'] == pg)].reset_index().drop('index', 1)
            # print pg
            df_2 = df2[['date_hour']].drop_duplicates(['date_hour'])
            for time in df_2['date_hour']:
                # print time
                if (time == df_2['date_hour'].min()):
                    idx = min(df2[df2['date_hour'] == time].index.tolist())
                else:
                    idx = min(df2[df2['date_hour'] < time].index.tolist())
                # print idx
                # Last 20 transaction calculations for the PG
                d2 = df2.iloc[idx:idx + 20, :]
                success_rate = d2['is_complete'].sum() / d2.shape[0]
                # Next 15 transaction calculations for the PG
                ##
                idx_cu = max(df2[df2['date_hour'] >= time].index.tolist())
                if idx_cu <= 15:
                    dcurr = df2.iloc[0:idx_cu + 1]
                else:
                    dcurr = df2.iloc[idx_cu - 15 + 1:idx_cu + 1, :]
                # dcurr = df2.iloc[idx_cu-15+1:idx_cu+1,:]
                sr_curr = dcurr['is_complete'].sum() / dcurr.shape[0]
                # add to dataframe
                df_sr = df_sr.append(
                    {'id': pi,
                     'pgname': pg,
                     'date_hour': time,
                     'successrate20': success_rate,
                     'current_sr15': sr_curr}, ignore_index=True)

    df_sr.to_csv(QUERY_OUT_DIR +'/' + filename_sr, index=False)
    return df_sr

def createDataset(df_sr, QUERY_OUT_DIR, filename_xy):
    float_formatter = lambda x: "%.4f" % x
    check_sr = [float_formatter(x) for x in [x[0] for x in Counter(df_sr.successrate20).most_common()]]
    #print check_sr, len(check_sr)

    df_bank_stat = pd.DataFrame(columns =['id','sr_X','sr_Y','sr_Y_median','sr_Y_std'])

    #count=1
    for pi in tqdm(df_sr['id'].unique()):
        sleep(0.01)
        for sr in check_sr:
            sr = float(sr)
            df1 = df_sr[(df_sr['id']==pi) & (df_sr['successrate20']==sr)]
            avg_curr_sr = df1['current_sr15'].mean()
            median_curr_sr = df1['current_sr15'].median()
            stdv_curr_sr = df1['current_sr15'].std()
            df_bank_stat = df_bank_stat.append(
                          {'id':pi,
                           #'pgname':pg,
                           'sr_X':sr,
                           'sr_Y':avg_curr_sr,
                           'sr_Y_median':median_curr_sr,
                           'sr_Y_std':stdv_curr_sr}, ignore_index=True)

    #print df_bank_stat.shape
    #print len(df_bank_stat[df_bank_stat['sr_Y'].isnull()])
    df_bank_stat = df_bank_stat[df_bank_stat['sr_Y'].notnull()]
    filename_xy = 'bank_sr_XY.csv'
    df_bank_stat['id']=df_bank_stat['id'].astype(int)
    df_bank_stat.to_csv(QUERY_OUT_DIR+'/'+ filename_xy,index = False)

