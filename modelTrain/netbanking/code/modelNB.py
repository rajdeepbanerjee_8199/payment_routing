import pandas as pd
import numpy as np
from collections import Counter
from tqdm import tqdm
import datetime
import scipy.stats as stats
from sklearn.gaussian_process import GaussianProcessRegressor
import pickle
import os, sys
from trainConfigNB import *


def gprModel(df, df_sr, bankid2nameMap, path, modelOutDir, QUERY_OUT_DIR):
    df = df.sort_values(['insert_date', 'insert_time'], ascending=[False, False]).reset_index().drop('index', 1)
    df['half_hr_period_1'] = df['half_hr_period'].map(lambda x: '0' + str(x) if len(str(x)) == 1 else str(x))
    df['date_hour'] = df.apply(lambda x: '%s%s' % (x['insert_date'], x['half_hr_period_1']), axis=1).astype(int)

    # BankName to bankId map
    df_map = pd.read_csv(bankid2nameMap)

    df_xy = pd.read_csv(QUERY_OUT_DIR+'/'+ filename_xy)
    df_xy = pd.merge(df_xy, df_map, how='left', on='id')
    df_xy.id = df_xy.id.astype(int)

    model_r2 = []
    for id in tqdm(df_xy.id.unique()):
        df0 = df_xy[df_xy['id'] == id]
        model_stats = {}
        # model_stats['bank'] = pi
        model_stats['bankId'] = id
        X = df0['sr_X'].to_numpy().reshape(-1, 1)
        y = df0['sr_Y'].to_numpy()
        gp = GaussianProcessRegressor()
        gp.fit(X, y)
        model_stats['score'] = gp.score(X, y)
        # dump model
        with open(path + str(id) + '.pkl', 'wb') as fid:
            pickle.dump(gp, fid)
            ####
        model_stats.update(model_stats)
        model_r2.append(model_stats)

    # save to file
    model_stats_final = pd.DataFrame(model_r2)
    model_stats_final.to_csv(modelOutDir + '/model_stats_final.csv', index=False,header=False)
    model_stats_final['bankId'].to_csv(modelOutDir + '/bankid_list.csv', index=False,header=False)

    # Bank level SR summary
    bank_pgs = pd.DataFrame()

    i = 1
    for pi in df.id.unique():
        dfb = df[df['id'] == pi]
        bank_pgs.at[i, 'bankId'] = pi
        bank_pgs.at[i, 'pgs'] = ','.join(list(dfb['payment_gateway_name'].unique()))
        bank_pgs['pgs'] = bank_pgs['pgs'].astype(object)
        # get last 10 days average sr per PG at bank level
        maxdate = dfb.insert_date.max()
        maxdate_10 = int(
            (datetime.datetime.strptime(str(maxdate), '%Y%m%d') - datetime.timedelta(days=10)).strftime("%Y%m%d"))
        dfb1 = dfb[dfb.insert_date >= maxdate_10]
        df_grp_rec = dfb1[['payment_gateway_name', 'is_complete']].groupby(['payment_gateway_name']).mean()[
            'is_complete']
        for pg in list(df_grp_rec.index):
            bank_pgs.at[i, 'mean_sr_' + pg] = df_grp_rec[pg]
        bank_pgs.at[i, 'pg_max'] = \
        dfb1[['payment_gateway_name', 'is_complete']].groupby(['payment_gateway_name']).mean()['is_complete'].idxmax()
        print (i, pi)
        i += 1

    # save to file
    bank_pgs = bank_pgs.fillna(0)
    bank_pgs['bankId'] = bank_pgs['bankId'].astype(int)
    bank_pgs.to_csv(modelOutDir + '/bank_pgs.csv', index=False)
    # bank_pgs['bank'].to_csv(sys.argv[1]+'/bank_list.csv',index = False)