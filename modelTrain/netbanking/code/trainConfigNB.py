import sys
import os

ddp_path=''
QUERY_NAME = 'pg_netbank_data_'
QUERY_USER='ajit.kumar@myntra.com'
bankIDListFile = '../bankid_list.csv'
bankid2nameMap = '../bankid2nameMap.csv'
filename_sr  = 'bank_level_success_rates.csv'
filename_xy = 'bank_sr_XY.csv'
numDaysData = 3

sqlquery = """select insert_date,payment_gateway_name,payment_option,
insert_time,payment_issuer,card_bank_name,bin_number,amount,is_saved_card, is_complete from fact_payment
WHERE insert_date BETWEEN CAST(CONVERT(varchar(8), DATEADD(DAY,-%d,getdate()), 112) AS BIGINT) 
and CAST(CONVERT(varchar(8), DATEADD(DAY,-1,getdate()), 112) AS BIGINT)
and payment_option = 'netbanking' """%(numDaysData)

MODEL_PARAM_DICT = dict(
    description = 'model train for Bank based pg routing for NetBanking',
    ddpPath = ddp_path,
    QUERY_NAME = QUERY_NAME,
    QUERY_USER = QUERY_USER,
    query = sqlquery,
    bankIDListFile = bankIDListFile,
    bankid2nameMap = bankid2nameMap,
    filename_sr = filename_sr,
    filename_xy = filename_xy
)

