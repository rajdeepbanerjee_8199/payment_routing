from __future__ import division
import pandas as pd
import numpy as np
import os
import datetime
import sys
from dataUtilsNB import *
from modelNB import *
from trainConfigNB import *

def getFeatures(query, QUERY_OUT_DIR, QUERY_USER, QUERY_NAME, filename , bankIDListFile, bankid2nameMap, filename_sr, filename_xy):
    runDDPquery(query, QUERY_OUT_DIR, QUERY_USER, QUERY_NAME)
    df = createFeatures1(QUERY_OUT_DIR, QUERY_NAME, filename, bankid2nameMap)
    df_sr = createFeatures2(df, bankIDListFile, QUERY_OUT_DIR, filename_sr)
    createDataset(df_sr, QUERY_OUT_DIR, filename_xy)


if __name__=='__main__':

    t = datetime.datetime.now().strftime("%Y%m%d")
    modelOutDir = '../model_out/' + t
    QUERY_OUT_DIR = modelOutDir + '/input'
    pickleFilesPath = modelOutDir + '/nb_pickle_files/'

    if not os.path.exists(modelOutDir):
        os.makedirs(modelOutDir)
    if not os.path.exists(QUERY_OUT_DIR):
        os.makedirs(QUERY_OUT_DIR)
    if not os.path.exists(pickleFilesPath):
        os.makedirs(pickleFilesPath)

    modelParams = MODEL_PARAM_DICT
    filename = modelParams['QUERY_NAME'] + t
    getFeatures(modelParams['query'], QUERY_OUT_DIR, modelParams['QUERY_USER'],  modelParams['QUERY_NAME'],
          filename, modelParams['bankIDListFile'], modelParams['bankid2nameMap'], modelParams['filename_sr'], modelParams['filename_xy'])

    df = pd.read_csv(QUERY_OUT_DIR + '/' + filename + '.csv')
    df_xy = pd.read_csv(QUERY_OUT_DIR + '/' + modelParams['filename_xy'])
    print(df.shape, df_xy.shape)

    print("Starting Model training.........")
    gprModel(df, df_xy, modelParams['bankid2nameMap'], pickleFilesPath, modelOutDir, QUERY_OUT_DIR)

