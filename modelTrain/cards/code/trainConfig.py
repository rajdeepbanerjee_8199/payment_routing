import sys
import os

ddp_path=''
QUERY_NAME = 'pg_bindata_'
QUERY_USER='ajit.kumar@myntra.com'
binListFile = '../bin_list200.csv'
filename_sr  = 'bin_level_success_rates.csv'
numDaysData = 1

sqlquery = """select insert_date,payment_gateway_name,payment_option,
insert_time,payment_issuer,card_bank_name,bin_number,amount,is_saved_card, is_complete from fact_payment
WHERE insert_date BETWEEN CAST(CONVERT(varchar(8), DATEADD(DAY,-%d,getdate()), 112) AS BIGINT) 
and CAST(CONVERT(varchar(8), DATEADD(DAY,-1,getdate()), 112) AS BIGINT)
and payment_option in ('creditcard','debitcard')"""%(numDaysData)

MODEL_PARAM_DICT = dict(
    description = 'model train for Bin based pg routing of cards',
    ddpPath = ddp_path,
    QUERY_NAME = QUERY_NAME,
    QUERY_USER = QUERY_USER,
    query = sqlquery,
    binListFile = binListFile,
    filename_sr = filename_sr
)

