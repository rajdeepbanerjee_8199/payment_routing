import pandas as pd
import numpy as np
from collections import Counter
import datetime
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import log_loss, confusion_matrix,precision_score,recall_score,accuracy_score

import pickle
import os, sys
#from trainConfig import *


def gbcModel(df, df_sr, path, modelOutDir):
    df = df[df['bin_number'].notnull()]
    df['bin_number'] = df['bin_number'].astype(int)
    df_sr['bin_number'] = df_sr['bin_number'].astype(int)
    df = df.sort_values(['insert_date', 'insert_time'], ascending=[False, False]).reset_index().drop('index', 1)
    df['half_hr_period_1'] = df['half_hr_period'].map(lambda x: '0' + str(x) if len(str(x)) == 1 else str(x))
    df['date_hour'] = df.apply(lambda x: '%s%s' % (x['insert_date'], x['half_hr_period_1']), axis=1).astype(int)

    pg_max_date = int((datetime.datetime.now() - datetime.timedelta(days=10)).strftime("%Y%m%d"))

    model_stats_final = pd.DataFrame()
    bin_pgs = pd.DataFrame()
    #print(df_sr.bin_number.unique())


    fixedBins = list(df_sr.bin_number.unique())
    top300 = [x[0] for x in Counter(df['bin_number']).most_common(300)]
    finalBins = list(set(fixedBins).union(top300))

    i = 1
    for bin in finalBins:
        # sleep(0.05)
        try:
            dfb = df[df['bin_number'] == bin]
            df_sr_bin = df_sr[df_sr['bin_number'] == bin]
            dfsr_trans_bin = pd.pivot_table(df_sr_bin.drop(['bin_number'], 1), values=['successrate20'],
                                            index=['date_hour'], columns=['pgname'])
            dfsr_trans_bin = dfsr_trans_bin.reset_index()
            cols = ['date_hour'] + ['sr_' + x for x in list(dfsr_trans_bin.columns.get_level_values(1)[1:])]
            dfsr_trans_bin.columns = cols
            dfsr_trans_bin = dfsr_trans_bin.fillna(0.00)
            df_final_bin = pd.merge(dfb, dfsr_trans_bin, how='left', on=['date_hour'])
            pgs = [col for col in list(df_final_bin) if col.startswith('sr_')]
            keep_cols = ['payment_gateway_name', 'is_complete'] + pgs
            df_final_bin = df_final_bin[keep_cols]
            data = df_final_bin
            ##
            bin_pgs.at[i, 'bin'] = bin
            # bin_pgs.set_value(i,'pgs',','.join(list(dfb['payment_gateway_name'].unique())))
            bin_pgs.at[i, 'pgs'] = ','.join(list(dfb['payment_gateway_name'].unique()))
            bin_pgs['pgs'] = bin_pgs['pgs'].astype(object)
            # df_grp = dfb[['payment_gateway_name','is_complete']].groupby(['payment_gateway_name']).mean()['is_complete']
            #
            # for pg in list(df_grp.index):
            #    bin_pgs.at[i,'mean_sr_'+pg] = df_grp[pg]
            # bin_pgs.at[i,'pg_max'] = dfb[['payment_gateway_name','is_complete']].groupby(['payment_gateway_name']).mean()['is_complete'].argmax()
            #
            dfb1 = dfb[dfb.insert_date >= pg_max_date]
            df_grp_rec = dfb1[['payment_gateway_name', 'is_complete']].groupby(['payment_gateway_name']).mean()[
                'is_complete']
            for pg in list(df_grp_rec.index):
                bin_pgs.at[i, 'mean_sr_' + pg] = df_grp_rec[pg]
            bin_pgs.at[i, 'pg_max'] = \
            dfb1[['payment_gateway_name', 'is_complete']].groupby(['payment_gateway_name']).mean()[
                'is_complete'].idxmax()
            #
            categorical_cols = ['payment_gateway_name']
            for name in categorical_cols:
                data = pd.concat([data, pd.get_dummies(data[name]).rename(columns=lambda x: name + '_' + str(x))],
                                 axis=1)
            # train test split
            y = data['is_complete']
            data = data.drop(['is_complete', 'payment_gateway_name'], axis=1)
            #
            # X_train,X_test,y_train,y_test = train_test_split(data.index,y,test_size=0.3)
            X_train, X_test, y_train, y_test = train_test_split(data.index, y, test_size=0.25, stratify=y)
            X_train = data.iloc[X_train]
            X_test = data.iloc[X_test]
            # training
            clf = GradientBoostingClassifier(n_estimators=100)
            clf.fit(X_train, y_train)
            # write model metrics
            model_stats_final.at[i, 'bin'] = bin
            #
            predGBC1 = clf.predict(X_test)
            model_stats_final.at[i, 'acc_scr'] = accuracy_score(y_test, predGBC1)
            model_stats_final.at[i, 'pr_scr'] = precision_score(y_test, predGBC1)
            model_stats_final.at[i, 're_scr'] = recall_score(y_test, predGBC1)
            #
            predGBC = clf.predict_proba(X_test)
            model_stats_final.at[i, 'log_loss'] = log_loss(y_test, predGBC[:, 1])
            # dump model
            with open(path + str(bin) + '.pkl', 'wb') as fid:
                pickle.dump(clf, fid)
                #
            print("Bin .pkl generated for: {} - {} ".format(i, bin))
            i += 1
        except:
            print("Bin .pkl NOT generated for: {}".format(bin))
            pass

    #save to file
    model_stats_final.to_csv(modelOutDir+'/model_stats_final.csv',index = False)
    bin_pgs = bin_pgs.fillna(0.0)
    bin_pgs['bin'] = bin_pgs['bin'].astype(int)
    bin_pgs.to_csv(modelOutDir+'/bin_pgs.csv',index = False,header=False)
    bin_pgs['bin'].to_csv(modelOutDir+'/bin_list200.csv',index = False,header=False)