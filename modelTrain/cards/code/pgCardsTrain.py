from __future__ import division
import pandas as pd
import numpy as np
import os
import datetime
import sys
from dataUtils import *
from model import *
from trainConfig import *

def getFeatures(query, QUERY_OUT_DIR, QUERY_USER, QUERY_NAME, filename , binListFile, filename_sr):
    runDDPquery(query, QUERY_OUT_DIR, QUERY_USER, QUERY_NAME)
    df = createFeatures1(QUERY_OUT_DIR, QUERY_NAME, filename)
    createFeatures2(df, binListFile, QUERY_OUT_DIR, filename_sr)
    #return df, df_sr

if __name__=='__main__':

    t = datetime.datetime.now().strftime("%Y%m%d")
    modelOutDir = '../model_out/' + t
    QUERY_OUT_DIR = modelOutDir + '/input'
    pickleFilesPath = modelOutDir + '/pickle_files/'

    if not os.path.exists(modelOutDir):
        os.makedirs(modelOutDir)
    if not os.path.exists(QUERY_OUT_DIR):
        os.makedirs(QUERY_OUT_DIR)
    if not os.path.exists(pickleFilesPath):
        os.makedirs(pickleFilesPath)

    modelParams = MODEL_PARAM_DICT
    filename = modelParams['QUERY_NAME'] + t

    getFeatures(modelParams['query'], QUERY_OUT_DIR, modelParams['QUERY_USER'],
                        modelParams['QUERY_NAME'], filename, modelParams['binListFile'], modelParams['filename_sr'])

    df = pd.read_csv(QUERY_OUT_DIR + '/' + filename + '.csv')
    df_sr = pd.read_csv(QUERY_OUT_DIR + '/' + modelParams['filename_sr'])
    print(df.shape, df_sr.shape)

    print("Starting Model training.........")
    gbcModel(df, df_sr, pickleFilesPath, modelOutDir)