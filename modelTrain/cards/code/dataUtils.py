import sys
import ddp, gzip
from time import sleep
from tqdm import tqdm
import os
import pandas as pd


def runDDPquery(query, QUERY_OUT_DIR, QUERY_USER, QUERY_NAME):
    ddp.start(query, QUERY_USER, QUERY_OUT_DIR, QUERY_NAME)

def halfHrPeriod(row):
    try:
        t1 = str(row['insert_time'])[:-2]
        if len(t1)==0:
            t1 = 0
        t1 = int(t1)
        t2 = int(str(row['insert_time'])[-2:])
        return t1*2 + t2//30
    except:
        return 0

def createFeatures1(QUERY_OUT_DIR, QUERY_NAME, filename, exclude_pgs = ["test", "icici"]):
    f0 = pd.read_table(gzip.open(QUERY_OUT_DIR + '/' + QUERY_NAME + ".gz"), sep=",", header=None)
    f0.columns = ['insert_date', 'payment_gateway_name', 'payment_option', 'insert_time', 'payment_issuer',
                  'card_bank_name', 'bin_number', 'amount', 'is_saved_card', 'is_complete']
    f0 = f0.reset_index()
    f0 = f0.drop('index', 1)
    f0['half_hr_period'] = f0.apply(halfHrPeriod, axis=1).astype(int)
    # include_pgs = ["axis", "citrus", "citrusblaze", "icici", "payu", "hdfc"]
    # f0 = f0[f0.payment_gateway_name.isin(include_pgs)]
    f0 = f0[~f0.payment_gateway_name.isin(exclude_pgs)]
    f0 = f0[f0.is_complete.isin([0, 1])]
    #print f0.payment_gateway_name.unique()
    f0.to_csv(QUERY_OUT_DIR + '/' + filename + '.csv', index=False)
    os.remove(QUERY_OUT_DIR + '/' + QUERY_NAME + ".gz")
    return f0


def createFeatures2(df, binListFile, QUERY_OUT_DIR, filename_sr):
    df = df[df['bin_number'].notnull()]
    df['bin_number'] = df['bin_number'].astype(int)
    binlist = pd.read_csv(binListFile, names=['bin_num'])

    df = df.sort_values(['insert_date', 'insert_time'], ascending=[False, False]).reset_index().drop('index', 1)
    df['half_hr_period_1'] = df['half_hr_period'].map(lambda x: '0' + str(x) if len(str(x)) == 1 else str(x))
    df['date_hour'] = df.apply(lambda x: '%s%s' % (x['insert_date'], x['half_hr_period_1']), axis=1).astype(int)
    df0 = df[['bin_number', 'payment_gateway_name', 'date_hour', 'insert_time', 'is_complete']]
    cols = ['bin_number', 'pgname', 'date_hour', 'successrate20', 'current_sr15']
    df_sr = pd.DataFrame(columns=(cols))

    for bin in tqdm(list(binlist.bin_num)):
        sleep(0.001)
        df1 = df0[df0['bin_number'] == bin].reset_index().drop('index', 1)
        # print df1.shape
        for pg in df1['payment_gateway_name'].unique():
            df2 = df1[(df1['payment_gateway_name'] == pg)].reset_index().drop('index', 1)
            # print pg
            df_2 = df2[['date_hour']].drop_duplicates(['date_hour'])
            for time in df_2['date_hour']:
                # print time
                if (time == df_2['date_hour'].min()):
                    idx = min(df2[df2['date_hour'] == time].index.tolist())
                else:
                    idx = min(df2[df2['date_hour'] < time].index.tolist())
                # print idx
                # Last 20 transaction calculations for the PG
                d2 = df2.iloc[idx:idx + 20, :]
                success_rate = d2['is_complete'].sum() / d2.shape[0]
                # Next 15 transaction calculations for the PG
                ##
                idx_cu = max(df2[df2['date_hour'] >= time].index.tolist())
                if idx_cu <= 15:
                    dcurr = df2.iloc[0:idx_cu + 1]
                else:
                    dcurr = df2.iloc[idx_cu - 15 + 1:idx_cu + 1, :]
                # dcurr = df2.iloc[idx_cu-15+1:idx_cu+1,:]
                sr_curr = dcurr['is_complete'].sum() / dcurr.shape[0]
                # add to dataframe
                df_sr = df_sr.append(
                    {'bin_number': bin,
                     'pgname': pg,
                     'date_hour': time,
                     'successrate20': success_rate,
                     'current_sr15': sr_curr}, ignore_index=True)

    #return df_sr
    df_sr.to_csv(QUERY_OUT_DIR + '/' + filename_sr, index=False)
